import React from 'react'
import '../App.css'
import List from './List'
import Input from './Input'
import UserLoc from './UserLoc'

import {OpenWeather} from '../api/OpenWeather'




class App extends React.Component{
    state = {location: '', data: []}

    fetchData = (q) => {
       
       OpenWeather.get(`?q=${q}&units=metric&appid=1108818747c4451dae3c1ff31813859b`)
       .then(response =>  {
        // handle success
        //console.log(response.data);
        this.setState({
            data: response.data.main
        })
      })
      .catch( error =>  {
        // handle error
        console.log(error);
      })
        
    }

    getLocation = (loc)=>{
        this.setState({
            location: loc
        })
    }

    

    render(){
      console.log(this.state.data)
        
        return(
            <div className="main">
                <div className="fromloc">
                {<UserLoc/>}
                </div>

                <div className="fromsearch">
                <Input data={this.getLocation} click={this.fetchData}/>
                <List temp={this.state.data.temp} humi={this.state.data.humidity} pres={this.state.data.pressure} />
                </div>
                
                
            </div>
            

        );
    }

}

export default App;