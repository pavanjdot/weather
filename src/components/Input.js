import React from 'react'
import '../App.css'

class Input extends React.Component{
    state = {input: ''}

    

    changeState(data){
        this.setState({
            input: data
        })
    }

    onclick(){
        this.props.data(this.state.input)
        this.props.click(this.state.input)

        
    }

    render(){
        //console.log(this.state)
        return(
            <div className="ui input inp">
                <input  type="text" onChange={(e) => {this.changeState(e.target.value)}} placeholder="Search..."></input>
                <button  className="ui primary basic button" onClick={() =>this.onclick()}>Submit</button>
            </div>
        );
    }
}

export default Input;