import React from 'react'
import {OpenWeather} from '../api/OpenWeather'
import List from './List'

class UserLoc extends React.Component{
    state = {lati: '', longi: '', data: [], name:''}


    fetchData(longi, latti){
        OpenWeather.get(`?lat=${longi}&lon=${latti}&units=metric&appid=1108818747c4451dae3c1ff31813859b`)
        .then(response =>  {
            // handle success
            //console.log(response);
            this.setState({
                data: response.data.main,
                name: response.data.name
            })
            
          })
          .catch( error =>  {
            // handle error
            console.log(error);
          })
         
    }


  componentDidMount(){
    window.navigator.geolocation.getCurrentPosition(
      (position)=> {
        let lati = position.coords.latitude
        let longi = position.coords.longitude
        this.fetchData(longi, lati)

      },
      (err)=> {
        console.log(err)
        
      }
      
    )
  }
    render(){
        console.log(this.state.data)
        console.log(this.state.name)
        return(
            <div>
                <List temp={this.state.data.temp} 
                      humi={this.state.data.humidity} 
                      pres={this.state.data.pressure}/>

              <div className="ui card">
                <div className="content">
    <div className="header">Location is {this.state.name == ''? '':   this.state.name}</div>
                </div>
            

            </div>
            </div>

            
        );
    }
}

export default UserLoc;