import React from 'react'

class List extends React.Component{
  
    
    render(){
        console.log(this.props.temp)
        return(
        <div>
            <div className="ui card">
  <div className="content">
    <div className="header">Weather Info </div>
  </div>
  
  <div className="content">
    <h2 className="ui sub header">Temparature: {this.props.temp} </h2>
    <h2 className="ui sub header">Humidity: {this.props.humi}</h2>
    <h2 className="ui sub header">Pressure: {this.props.pres} </h2>
    
  </div>

</div>
        </div>
        );
            
    }
}

export default List;